require_relative '../lib/price.rb'
RSpec.describe Price do 
    # free shipping
    price_html_block_0 = '<div class="a-column a-span2 olpPriceColumn" role="gridcell"> <span class="a-size-large a-color-price olpOfferPrice a-text-bold"> $63.07 </span> <p class="olpShippingInfo"> <span class="a-color-secondary"> &amp; <b>FREE Shipping</b> <span class="olpEstimatedTaxText"> + $0.00 estimated tax </span> </span> </p> </div>'
    # not free shipping
    price_html_block_1 = '<div class="a-column a-span2 olpPriceColumn" role="gridcell"> <span class="a-size-large a-color-price olpOfferPrice a-text-bold"> $55.94 </span> <p class="olpShippingInfo"> <span class="a-color-secondary"> + <span class="olpShippingPrice">$3.99</span> <span class="olpShippingPriceText">shipping</span> <span class="olpEstimatedTaxText"> + $0.00 estimated tax </span> </span> </p> </div>'
    describe ".parse_sale_price" do
        context '-parse raw html string' do
            it "should get sale price 63.07" do
                price = Price.parse_sale_price(price_html_block_0)
                expect(price).to eq(63.07)
            end

            it "should get sale price 55.94" do
                price = Price.parse_sale_price(price_html_block_1)
                expect(price).to eq(55.94)
            end
        end

        context '-parse nokogiri doc' do
            it "should get sale price 63.07" do
                doc = Nokogiri::HTML(price_html_block_0)
                price = Price.parse_sale_price(doc)
                expect(price).to eq(63.07)
            end

            it "should get sale price 55.94" do
                doc = Nokogiri::HTML(price_html_block_1)
                price = Price.parse_sale_price(doc)
                expect(price).to eq(55.94)
            end
        end
    end

    describe '.parse_shipping_fee' do
        context 'parse raw html string' do
            it "should get shipping fee 3.99" do
                shipping_fee = Price.parse_shipping_fee(price_html_block_1)
                expect(shipping_fee).to eq(3.99)
            end

            it "free shipping get shipping fee 0" do
                shipping_fee = Price.parse_shipping_fee(price_html_block_0)
                expect(shipping_fee).to eq(0)
            end
        end

        context 'pass nokogiri doc' do
            it "should get shipping fee 3.99" do
                doc = Nokogiri::HTML(price_html_block_1)
                shipping_fee = Price.parse_shipping_fee(doc)
                expect(shipping_fee).to eq(3.99)
            end
            
            it "free shipping get shipping fee 0" do
                doc = Nokogiri::HTML(price_html_block_0)
                shipping_fee = Price.parse_shipping_fee(doc)
                expect(shipping_fee).to eq(0)
            end
        end
    end
end