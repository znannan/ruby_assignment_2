require_relative '../lib/condition.rb'
RSpec.describe Condition do 
    
    condition_html_block = '<div id="offerCondition" class="a-section a-spacing-small"><span id="olpUsed" class="a-size-medium olpCondition a-text-bold">    Used<span id="offerSubCondition">    Good</span></span></div>'
    
    describe ".parse condition" do
        
        it "should get condition 'used-good'" do
            condition = Condition.parse(condition_html_block)
            expect(condition).to eq('Used-Good')
        end

        

        
    end
end