require_relative '../lib/rating.rb'
RSpec.describe Seller_rating do 
    # normal seller
    seller_rating_html_block_0 = '<div class="a-column a-span2 olpSellerColumn" role="gridcell"><h3 class="a-spacing-none olpSellerName">        <span class="a-size-medium a-text-bold">            <a href="/gp/aag/main/ref=olp_merch_name_4?ie=UTF8&amp;asin=0262510871&amp;isAmazonFulfilled=1&amp;seller=AFFCP3WPSKXSP">TEXTBookAMAZING</a>        </span></h3>                <p class="a-spacing-small">                            <i class="a-icon a-icon-star a-star-5"><span class="a-icon-alt">5 out of 5 stars</span></i>                            <a href="/gp/aag/main/ref=olp_merch_rating_4?ie=UTF8&amp;asin=0262510871&amp;isAmazonFulfilled=1&amp;seller=AFFCP3WPSKXSP"><b>98% positive</b></a> over the past 12 months. (15,539 total ratings)                        <br>                </p>                           </div>'
    # amazon 
    seller_rating_html_block_1 = '<div class="a-column a-span2 olpSellerColumn" role="gridcell"><h3 class="a-spacing-none olpSellerName"> <img alt="Amazon.com" src="https://images-na.ssl-images-amazon.com/images/I/01dXM-J1oeL.gif"></h3> </div>'
    describe ".parse_seller_rating" do
        it "should get normal seller rating" do
            seller_rating = Seller_rating.parse(seller_rating_html_block_0)
            expect(seller_rating).to eq("98% positive")
        end

        it "should get Amazon rating" do
            seller_rating = Seller_rating.parse(seller_rating_html_block_1)
            expect(seller_rating).to eq("Amazon")
        end
       
    end

end