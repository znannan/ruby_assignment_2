require 'nokogiri'
class Price
    def initilaize(price_html)
        @price_html = price_html
    end

    def self.parse(price_html)
        doc = Nokogiri::HTML(price_html)
        sale_price = doc.at_css('.olpOfferPrice')
        shipping_fee = doc.at_css('.olpShippingPrice')
        @sale_price = Price.parse_sale_price(sale_price)
        @parse_shipping_fee = Price.parse_shipping_fee(shipping_fee)
    end

    def self.parse_sale_price(sale_price)
        if(sale_price.is_a?(String))
            doc = Nokogiri::HTML(sale_price)
        else
            doc = sale_price
        end
        doc.text.gsub(/[^0-9\.]/,'').to_f.round(2)
        
    end

    def self.parse_shipping_fee(shipping_fee)
        if(shipping_fee.is_a?(String))
            doc = Nokogiri::HTML(shipping_fee)
            free_shipping = doc.at_css('b')
        else
            doc = shipping_fee
        end
        if(doc.at_css('b'))
            0
        else
            shipping_fee_doc = doc.at_css('.olpShippingPrice')
            shipping_fee_doc.text.gsub(/[^0-9\.]/,'').to_f.round(2)
        end
    end
end


